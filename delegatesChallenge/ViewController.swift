//
//  ViewController.swift
//  delegatesChallenge
//
//  Created by John Peppe on 27.05.2021.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    let zipCodeDelegate = ZipCodeFieldDelegate()
    let cashDelegate = CashFieldDelegate()
    
    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var moneyField: UITextField!
    @IBOutlet weak var lockField: UITextField!
    @IBOutlet weak var textSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.zipCodeField.delegate = self.zipCodeDelegate
        self.moneyField.delegate = self.cashDelegate
        self.lockField.delegate = self
    
        self.textSwitch.setOn(false, animated: false)
        
    }
    
    // text field delgates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.textSwitch.isOn
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
    
    // actions
    
    @IBAction func toggleEditor(_ sender: Any) {
        
        if !(sender as! UISwitch).isOn {
            self.lockField.resignFirstResponder()
        }
    }
    
    
}

